Source: libstring-truncate-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libsub-exporter-perl <!nocheck>,
                     libsub-install-perl <!nocheck>,
                     perl
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libstring-truncate-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libstring-truncate-perl.git
Homepage: https://metacpan.org/release/String-Truncate
Rules-Requires-Root: no

Package: libstring-truncate-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libsub-exporter-perl,
         libsub-install-perl
Description: Perl module to truncate long strings
 String::Truncate is a Perl module useful for truncating any long string
 sequence and marking the place where any text has been elided (deleted).
 You simply tell the module the maximum string width and where to elide
 strings (at the right side, left side, in the middle or at the ends). It
 also supports a customized marker for elisions, but defaults to the most
 common symbol, the ellipsis (...). It can also prefer to break strings at
 whitespace where possible.
